       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. WUTTIWAT.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUE.
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YEAR-OF-BIRTH PIC 9(4).
              10 EMP-MONTH-OF-BIRTH PIC 9(2).
              10 EMP-DAY-OF-BIRTH PIC 9(2).
           05 EMP-GENDER PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN EXTEND EMP-FILE
           MOVE "123456789" TO EMP-SSN 
           MOVE "DEEJA" TO EMP-SURNAME 
           MOVE "WUTTIWAT" TO EMP-FORENAME
           MOVE "20010116" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS 

           MOVE "987654321" TO EMP-SSN 
           MOVE "CARBON" TO EMP-SURNAME 
           MOVE "WATCHARANAT" TO EMP-FORENAME
           MOVE "20010413" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS 

           MOVE "432987651" TO EMP-SSN 
           MOVE "MEK" TO EMP-SURNAME 
           MOVE "ARTHON" TO EMP-FORENAME
           MOVE "20000615" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS 
           CLOSE EMP-FILE   

           GOBACK 
           .